/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thidarat.inheritance;

/**
 *
 * @author User
 */
public class TestAnimal {
    public static void main(String[] args) {
        Animal animal =new Animal("Annimal","White",0);
         animal.walk();
         animal.speak(); 
       
        
        Dog dang =new Dog("Dang","Black&White");
        dang.walk();
        dang.speak();
        
        Dog to =new Dog("To","Orange&Black");
        to.walk();
        to.speak();
        
        Dog mome =new Dog("Mome","Black&White");
        mome.walk();
        mome.speak();
        
        Dog bat =new Dog("Bat","Black&White");
        bat.walk();
        bat.speak();
        
        
        Cat zero =new Cat("Zero","Orange");
        zero.walk();
        zero.speak();
        
        Duck zom =new Duck("som","Orange");
        zom.walk();
        zom.fly();
        zom.speak();
        
        Duck gabgab =new Duck("Gabgab","Black");
        gabgab.walk();
        gabgab.fly();
        gabgab.speak();
        
        System.out.println("Zom is Animal: "+ (zom instanceof Animal));
        System.out.println("Zom is Animal: "+ (zom instanceof Duck));
        
        
        Animal[] animals={dang,zero,zom,gabgab,to,mome,bat};
        for (int i=0;i<animals.length;i++){
            
            if(animals[i] instanceof Duck){
                Duck duck =(Duck)animals[i];
                duck.fly();
            }animals[i].walk();
            animals[i].speak();
        }
    }
    
    
}
