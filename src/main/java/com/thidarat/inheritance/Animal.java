/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thidarat.inheritance;

/**
 *
 * @author User
 */
public class Animal {
    protected String name;
    protected int numOfLegs=0;
    protected String color;
    
    public Animal(String name ,String color,int numberOfLegs){
         System.out.println("Animal created");
        this.name=name;
        this.color=color;
        this.numOfLegs=numberOfLegs;
        
    }
    public void walk(){
        System.out.println("Annimal walk");
       
    }
    public void speak(){
        System.out.println("Annimal speak");
        System.out.println("name: "+this.name+" color: "+this.color+
                " numberOfLegs: "+this.numOfLegs);
        System.out.println("------------------------");
    }

    public String getColor() {
        return color;
    }

    public String getName() {
        return name;
    }

    public int getNumOfLegs() {
        return numOfLegs;
    }
    
}
